/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.mcfadyen.saas.web.rest.dto;
