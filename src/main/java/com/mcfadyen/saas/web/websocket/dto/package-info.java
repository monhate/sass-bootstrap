/**
 * Data Access Objects used by WebSocket services.
 */
package com.mcfadyen.saas.web.websocket.dto;
