(function () {
    'use strict';

    angular
        .module('saasbootstrapApp')
        .factory('Register', Register);

    Register.$inject = ['$resource'];

    function Register ($resource) {
        return $resource('api/register', {}, {});
    }
})();
