(function() {
    'use strict';

    angular
        .module('saasbootstrapApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
